![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Double Sideband Suppressed Carrier (DSBSC) <img src="DSBSC.png" alt="DSBSC" style="zoom:150%;" />

<img src="DSBSC readme.png" alt="DSBSC readme" style="zoom:150%;" />

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
