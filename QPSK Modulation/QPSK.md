![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Quadrature Phase Shift Keying (QPSK) ![QPSK](QPSK.png)

<img src="QPSK readme.png" alt="QPSK readme" style="zoom:100%;" />

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
