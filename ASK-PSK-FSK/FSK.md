![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Frequency Shift Keying (FSK)<img src="FSK.png" alt="FSK" style="zoom:200%;" />

<img src="FSK readme.png" alt="FSK readme" style="zoom:100%;" />

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
