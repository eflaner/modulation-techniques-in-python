![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Amplitude Shift Keying (ASK) / On-Off Keying (OOK)![ASK](ASK.png)

<img src="ASK readme.png" alt="ASK readme" style="zoom:67%;" />

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
