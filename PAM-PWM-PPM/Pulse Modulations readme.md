![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Pulse Modulations (PAM, PWM & PPM) ![PModulations](PMod.png)

<img src="Pulse Modulations readme.png" alt="PAM, PWM & PPM readme" style="zoom:100%;" />

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
