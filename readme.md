![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Implementation of Modulation Techniques in Python

The Modulation Techniques which are mostly used in communication systems are:
* [Basic Analog Modulation](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/ASK-PSK-FSK/1.%20ASK%20FSK%20and%20PSK%20Modulation%20and%20Demodulation.html)
  - [Amplitude Shift Keying (ASK or OOK)](ASK-PSK-FSK/ASK.md)
  - [Phase Shift Keying (PSK)](ASK-PSK-FSK/PSK.md)
  - [Frequency Shift Keying](ASK-PSK-FSK/FSK.md)
* [Double Sideband Suppressed Carrier (DSBSC)](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/DSBSC/2.%20DSB-SC.html) - [readme](DSBSC/DSBSC.md)
* [Pulse Modulation](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/PAM-PWM-PPM/3.%20PAM%20PWM%20and%20PPM.html) - [readme](PAM-PWM-PPM/Pulse Modulations readme.md)
  - Pulse Amplitude Modulation
  - Pulse Width Modulation
  - Pulse Position Modulation
* [Pulse Coded Modulation and Demodulation](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/PCM/4.%20PCM%20Modulation%20and%20Demodulation.html) - [readme](PCM/PCM.md)
* [Delta Modulation](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/Delta%20Modulation/5.%20Delta%20Modulation.html) - [readme](Delta Modulation/DM.md)
* [QPSK Modulation and Demodulation ](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/QPSK%20Modulation/6.%20QPSK%20Modulation%20and%20Demodulation.html)- [readme](QPSK Modulation/QPSK.md)
* [DPSK Modulation and Demodulation ](https://glcdn.githack.com/eflaner/modulation-techniques-in-python/raw/master/DPSK%20Modulation/7.%20DPSK%20Modulator%20and%20Demodulator.html)- [readme](DPSK Modulation/DPSK.md)

There are more Techniques than above specified ones, but these were the ones that I've implemented as part of the 'Communication Systems' Laboratory

### ~~Prerequisites~~

The files present in this repo are  html version of `.ipynb` files, which were generated using Jupyter.

### How to simulate
If you don't have Jupyter installation, No Worries, you can still simulate the `.html` file contents using:
* [JupyterLab](https://jupyter.org/try)
* [Binder](https://mybinder.org/)
* [nbviewer](https://nbviewer.jupyter.org/)
* [Google's Colab](https://research.google.com/colaboratory)

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).

## Acknowledgements

*As a student, I've copied snippets from various authors, as the ultimate aim was to get the desired output. Now i regret, for not making note of authors from whom I've borrowed. I sincerely acknowledge their work and as token of gratitude the work done in this repo is dedicated to all of them.*